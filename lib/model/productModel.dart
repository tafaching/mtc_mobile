import 'package:go_kart/model/base_model.dart';

class ProductModel with ModelMapper {
  int id;
  String sku;
  String productType;
  dynamic affiliateLink;
  int userId;
  int categoryId;
  int subcategoryId;
  dynamic childcategoryId;
  dynamic attributes;
  String name;
  String slug;
  String photo;
  String thumbnail;
  dynamic file;
  String size;
  String sizeQty;
  String sizePrice;
  List<String> color;
  String price;
  String previous_price;
  String details;
  dynamic stock;
  String policy;
  int status;
  int views;
  List<String> tags;
  String features;
  String colors;
  int productCondition;
  String ship;
  int isMeta;
  List<String> metaTag;
  String metaDescription;
  String youtube;
  String type;
  String license;
  String licenseQty;
  dynamic link;
  dynamic platform;
  dynamic region;
  dynamic licenceType;
  dynamic measure;
  int featured;
  int best;
  int top;
  int hot;
  int latest;
  int big;
  int trending;
  int sale;
  String createdAt;
  String updatedAt;
  int isDiscount;
  dynamic discountDate;
  List<String> wholeSellQty;
  List<String> wholeSellDiscount;
  int isCatalog;
  int catalogId;

  ProductModel(
      {this.id,
      this.sku,
      this.productType,
      this.affiliateLink,
      this.userId,
      this.categoryId,
      this.subcategoryId,
      this.childcategoryId,
      this.attributes,
      this.name,
      this.slug,
      this.photo,
      this.thumbnail,
      this.file,
      this.size,
      this.sizeQty,
      this.sizePrice,
      this.color,
      this.price,
      this.previous_price,
      this.details,
      this.stock,
      this.policy,
      this.status,
      this.views,
      this.tags,
      this.features,
      this.colors,
      this.productCondition,
      this.ship,
      this.isMeta,
      this.metaTag,
      this.metaDescription,
      this.youtube,
      this.type,
      this.license,
      this.licenseQty,
      this.link,
      this.platform,
      this.region,
      this.licenceType,
      this.measure,
      this.featured,
      this.best,
      this.top,
      this.hot,
      this.latest,
      this.big,
      this.trending,
      this.sale,
      this.createdAt,
      this.updatedAt,
      this.isDiscount,
      this.discountDate,
      this.wholeSellQty,
      this.wholeSellDiscount,
      this.isCatalog,
      this.catalogId});

  @override
  ModelMapper fromMap(map) {
    id = map["id"] ?? '';
    sku = map["sku"] ?? '';
    productType = map["productType"] ?? '';
    affiliateLink = map["affiliateLink"] ?? '';
    userId = map["userId"] ;
    categoryId = map["categoryId"] ;
    subcategoryId = map["subcategoryId"];
    childcategoryId = map["childcategoryId"] ?? '';
    attributes = map["attributes"] ?? '';
    name = map["name"] ?? '';
    slug = map["slug"] ?? '';
    photo = map["photo"] ?? '';
    thumbnail = map["thumbnail"] ?? '';
    file = map["file"] ?? '';
    size = map[dynamic];
    sizeQty = map["sizeQty"] ?? '';
    sizePrice = map["sizePrice"] ?? '';
    color = map[dynamic];
    price = map["price"] ?? 0.00;
    previous_price = map["previous_price"]?? 0.00;
    details = map["details"] ?? '';
    stock = map["stock"] ?? '';
    policy = map["policy"] ?? '';
    status = map["status"] ?? '';
    views = map["views"] ?? '';
    tags = map[dynamic];
    features = map[dynamic];
    colors = map[dynamic];
    productCondition = map["productCondition"];
    ship = map["ship"] ?? '';
    isMeta = map[dynamic];
    metaTag = map["metaTag"];
    metaDescription = map["metaDescription"] ?? '';
    youtube = map["youtube"] ?? '';
    type = map["type"] ?? '';
    license = map[dynamic] ;
    licenseQty = map["licenseQty"] ?? '';
    link = map["link"] ?? '';
    platform = map["platform"] ?? '';
    region = map["region"] ?? '';
    licenceType = map["licenceType"] ?? '';
    measure = map["measure"] ?? '';
    featured = map["featured"] ?? '';
    best = map["best"] ?? '';
    top = map["top"] ?? '';
    hot = map["hot"] ?? '';
    latest = map["latest"] ?? '';
    big = map["big"] ?? '';
    trending = map["trending"] ?? '';
    sale = map["sale"] ?? '';
    createdAt = map["createdAt"] ?? '';
    updatedAt = map["updatedAt"] ?? '';
    isDiscount = map["isDiscount"];
    discountDate = map["discountDate"] ?? '';
    wholeSellQty = map["wholeSellQty"];
    wholeSellDiscount = map["wholeSellDiscount"];
    isCatalog = map["isCatalog"];
    catalogId = map["catalogId"];
    return this;
  }

  @override
  ModelMapper newInstance() {
    return ProductModel();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'sku': sku,
      'productType': productType,
      'affiliateLink': affiliateLink,
      'userId': userId,
      'categoryId': categoryId,
      'subcategoryId': subcategoryId,
      'childcategoryId': childcategoryId,
      'attributes': attributes,
      'name': name,
      'slug': slug,
      'photo': photo,
      'thumbnail': thumbnail,
      'file': file,
      'size': size,
      'sizeQty': sizeQty,
      'sizePrice': sizePrice,
      'color': color,
      'price': price,
      'previous_price': previous_price,
      'details': details,
      'stock': stock,
      'policy': policy,
      'status': status,
      'views': views,
      'tags': tags,
      'features': features,
      'colors': colors,
      'productCondition': productCondition,
      'ship': ship,
      'isMeta': isMeta,
      'metaTag': metaTag,
      'metaDescription': metaDescription,
      'youtube': youtube,
      'type': type,
      'license': license,
      'licenseQty': licenseQty,
      'link': link,
      'platform': platform,
      'region': region,
      'licenceType': licenceType,
      'measure': measure,
      'featured': featured,
      'best': best,
      'top': top,
      'hot': hot,
      'latest': latest,
      'big': big,
      'trending': trending,
      'sale': sale,
      'createdAt': createdAt,
      'updatedAt': updatedAt,
      'isDiscount': isDiscount,
      'discountDate': discountDate,
      'wholeSellQty': wholeSellQty,
      'wholeSellDiscount': wholeSellDiscount,
      'isCatalog': isCatalog,
      'catalogId': catalogId,
    };
  }
}
