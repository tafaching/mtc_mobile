import 'package:go_kart/model/base_model.dart';

class Wish with ModelMapper {
  int id;
  String name;
  String price;
  String size;
  int user_id;
  String photo;
  int product_id;

  Wish({
    this.id,
    this.name,
    this.price,
    this.size,
    this.user_id,
    this.photo,
    this.product_id,
  });

  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    name = map['name'];
    price = map['price'];
    size = map['size'];
    user_id = map['user_id'];
    photo = map['photo'];
    product_id = map['product_id'];

    return this;
  }

  @override
  ModelMapper newInstance() {
    return Wish();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'size': size,
      'user_id': user_id,
      'photo': photo,
      'product_id': product_id,
    };
  }
}
