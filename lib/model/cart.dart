import 'package:go_kart/model/base_model.dart';

class Cart with ModelMapper {
  int id;
  String name;
  String price;
  String size;
  String userId;
  String photo;
  String createdAt;
  String updatedAt;

  Cart(
      {this.id,
      this.name,
      this.price,
      this.size,
      this.userId,
      this.photo,
      this.createdAt,
      this.updatedAt});


  @override
  ModelMapper fromMap(map) {
    id = map['id'];
    name = map['name'];
    price = map['price'];
    size = map['size'];
    userId = map['userId'];
    photo = map['photo'];
    createdAt = map['createdAt'];
    updatedAt = map['updatedAt'];
    return this;
  }

  @override
  ModelMapper newInstance() {

    return Cart();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name':name,
      'price':price,
      'size':size,
      'userId':userId,
      'photo':photo,
      'createdAt':createdAt,
      'updatedAt':updatedAt,
    };
  }
}
