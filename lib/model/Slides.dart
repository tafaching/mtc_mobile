import 'base_model.dart';

class Slides with ModelMapper {
  int id;
  String subtitleText;
  String subtitleSize;
  String subtitleColor;
  String subtitleAnime;
  String titleText;
  String titleSize;
  String titleColor;
  String titleAnime;
  String detailsText;
  String detailsSize;
  String detailsColor;
  String detailsAnime;
  String photo;
  String position;
  String link;

  Slides(
      {this.id,
      this.subtitleText,
      this.subtitleSize,
      this.subtitleColor,
      this.subtitleAnime,
      this.titleText,
      this.titleSize,
      this.titleColor,
      this.titleAnime,
      this.detailsText,
      this.detailsSize,
      this.detailsColor,
      this.detailsAnime,
      this.photo,
      this.position,
      this.link});

  @override
  ModelMapper fromMap(map) {
    id = map["id"] ?? '';
    subtitleText = map["subtitleText"] ?? '';
    subtitleSize = map["subtitleSize"] ?? '';
    subtitleColor = map["subtitleColor"] ?? '';
    subtitleAnime = map["subtitleAnime"] ?? '';
    titleText = map["titleText"] ?? '';
    titleSize = map["titleSize"] ?? '';
    titleColor = map["titleColor"] ?? '';
    titleAnime = map["titleAnime"] ?? '';
    detailsText = map["detailsText"] ?? '';
    detailsSize = map["detailsSize"] ?? '';
    detailsColor = map["detailsColor"] ?? '';
    detailsAnime = map["detailsAnime"] ?? '';
    photo = map["photo"] ?? '';
    position = map["position"] ?? '';
    link = map["link"];
    return this;
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'subtitleText': subtitleText,
      'subtitleSize': subtitleSize,
      'subtitleColor': subtitleColor,
      'subtitleAnime': subtitleAnime,
      'titleText': titleText,
      'titleSize': titleSize,
      'titleColor': titleColor,
      'titleAnime': titleAnime,
      'detailsText': detailsText,
      'detailsSize': detailsSize,
      'detailsColor': detailsColor,
      'detailsAnime': detailsAnime,
      'photo': photo,
      'position': position,
      'link': link
    };
  }

  @override
  ModelMapper newInstance() {
    return Slides();
  }
}
