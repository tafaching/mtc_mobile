import 'package:go_kart/model/base_model.dart';

class Category with ModelMapper {
  int id;
  String name;
  String slug;
  int status;
  String photo;
  int isFeatured;
  String image;

  Category(
      {this.id,
      this.name,
      this.slug,
      this.status,
      this.photo,
      this.isFeatured,
      this.image});

  @override
  ModelMapper fromMap(map) {
    id = map["id"] ?? '';
    name = map["name"] ?? '';
    slug = map["slug"] ?? '';
    status = map["status"] ?? '';
    photo = map["photo"] ?? '';
    isFeatured = map["isFeatured"] ?? '';
    image = map["image"] ?? '';
    return this;
  }

  @override
  ModelMapper newInstance() {
    return Category();
  }

  @override
  Map toJson() {
    return {
      'id': id,
      'name': name,
      'slug': slug,
      'status': status,
      'photo': photo,
      'isFeatured': isFeatured,
      'image': image
    };
  }
}
