import 'package:flutter/material.dart';
import 'package:go_kart/pages/spashscreen.dart';
import 'package:go_kart/viewModel/cart_view_model.dart';
import 'package:go_kart/viewModel/category_view_model.dart';
import 'package:go_kart/viewModel/orders_view_model.dart';
import 'package:go_kart/viewModel/product_view_model.dart';
import 'package:go_kart/viewModel/slides_view_model.dart';
import 'package:go_kart/viewModel/wish_view_model.dart';
import 'package:provider/provider.dart';

void main() => runApp(MultiProvider(providers: [
      ChangeNotifierProvider(
        create: (context) => CategoryViewModel(),
      ),
      ChangeNotifierProvider(
        create: (context) => ProductViewModel(),
      ),
  ChangeNotifierProvider(
    create: (context) => SlidesViewModel(),
  ),

  ChangeNotifierProvider(
    create: (context) => OrdersViewModel(),
  ),

  ChangeNotifierProvider(
    create: (context) => CartViewModel(),
  ),

  ChangeNotifierProvider(
    create: (context) => WishViewModel(),
  ),
    ], child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mtc Mall',
      theme: ThemeData(
          primarySwatch: Colors.green,
          primaryColor: Colors.green,
          // accentColor: const Color(0xFFFDE400),
          accentColor: Colors.green,
          primaryColorLight: const Color(0xFFFDE400)),
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SplashScreen(),
    );
  }
}
