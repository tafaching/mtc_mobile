import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:go_kart/Animation/slide_left_rout.dart';
import 'package:go_kart/pages/cart/cartPage.dart';
import 'package:go_kart/pages/wish_list/wishlist.dart';

import 'get_mobile_and_tablets.dart';

class MobileAndTablets extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text("Electronics"),
        titleSpacing: 0.0,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.favorite,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context, SlideLeftRoute(page: WishlistPage()));
            },
          ),
          IconButton(
            icon: Badge(
              badgeContent: Text('3'),
              badgeColor: Theme.of(context).primaryColorLight,
              child: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Navigator.push(context, SlideLeftRoute(page: CartPage()));
            },
          ),
        ],
      ),
      backgroundColor: const Color(0xFFF1F3F6),
      body: GetMobileAndTablets(),
    );
  }
}
