import 'package:flutter/material.dart';
import 'package:go_kart/pages/category/top_offers_pages/filter_row.dart';

import 'tv_and_appliances_grid_view.dart';

class GetTvAndAppliances extends StatefulWidget {
  @override
  _BestSellerState createState() => _BestSellerState();
}

class _BestSellerState extends State<GetTvAndAppliances> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        FilterRow(),
        Divider(
          height: 1.0,
        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: TvAndAppliancesGridView()),
      ],
    );
  }
}
