import 'package:flutter/material.dart';
import 'package:go_kart/viewModel/orders_view_model.dart';
import 'package:provider/provider.dart';

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.7;
    double height = MediaQuery.of(context).size.height;
    Provider.of<OrdersViewModel>(context, listen: false).getOrderByUserId();
    {
      Container _checkStatus(status) {
        //'pending','processing','completed','declined','on delivery'

        if (status == "pending") {
          return Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5.0)),
            ),
            child: Text(
              'Pending',
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            ),
          );
        } else if (status == "processing") {
          return Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Colors.orange,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5.0)),
            ),
            child: Text(
              'Processing',
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            ),
          );
        } else if (status == "completed") {
          return Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Colors.greenAccent,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5.0)),
            ),
            child: Text(
              'Completed',
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            ),
          );
        } else if (status == "declined") {
          return Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5.0)),
            ),
            child: Text(
              'Declined',
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            ),
          );
        } else {
          return Container(
            padding: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Colors.green,
              borderRadius: BorderRadius.only(topRight: Radius.circular(5.0)),
            ),
            child: Text(
              'on delivery',
              style: TextStyle(color: Colors.white, fontSize: 12.0),
            ),
          );
        }
      }

      return Scaffold(
        appBar: AppBar(
          title: Text('My Orders'),
          titleSpacing: 0.0,
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Consumer<OrdersViewModel>(
            builder: (context, ordersViewModel, child) {
          return ListView.builder(
            itemCount: ordersViewModel.orderList.length,
            itemBuilder: (context, index) {
              final item = ordersViewModel.orderList[index];
              return Container(
                // height: (height / 4.5),
                child: Card(
                    elevation: 5.0,
                    child: Stack(
                      children: <Widget>[
                        Positioned(
                          top: 0,
                          right: 0,
                          child: _checkStatus(item.status),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              top: (height / 40.0), bottom: (height / 40.0)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.all(10.0),
                                    width: (width - 20.0),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        RichText(
                                          text: TextSpan(
                                            text: 'Order No:  ',
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                color: Colors.black),
                                            children: <TextSpan>[
                                              TextSpan(
                                                  text: item.order_number,
                                                  style: TextStyle(
                                                      fontSize: 15.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black)),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10.0,
                                        ),
                                      ],
                                    ),
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            'Items:',
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Text(
                                            '${item.totalQty}',
                                            style: TextStyle(
                                                fontSize: 15.0,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 7.0,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            'Total Price:',
                                            style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Text(
                                            'R${item.pay_amount}',
                                            style: TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 7.0,
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                          color: Colors.green,
                                          borderRadius: BorderRadius.only(
                                              topRight: Radius.circular(5.0)),
                                        ),
                                        child: Text(
                                          'View details',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 12.0),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    )),
              );
            },
          );
        }),
      );
    }
  }
}
