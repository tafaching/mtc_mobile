import 'package:flutter/material.dart';
import 'package:go_kart/constants/Constant.dart';
import 'package:go_kart/viewModel/wish_view_model.dart';
import 'package:provider/provider.dart';

class WishlistPage extends StatefulWidget {
  @override
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {
  int wishlistItem = 4;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.7;
    double height = MediaQuery.of(context).size.height;
    Provider.of<WishViewModel>(context, listen: false).getWishByUserId();
    {
      return Scaffold(
        appBar: AppBar(
          title: Text('My WishList'),
          titleSpacing: 0.0,
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Consumer<WishViewModel>(builder: (context, wishViewModel, child) {
          return ListView.builder(
            itemCount: wishViewModel.wishList.length,
            itemBuilder: (context, index) {
              final item = wishViewModel.wishList[index];
              return Dismissible(
                key: Key('$item'),
                onDismissed: (direction) {
                  setState(() {
                    wishViewModel.wishList.removeAt(index);
                    wishlistItem--;
                  });

                  // Then show a snackbar.
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text("Item Removed")));
                },
                // Show a red background as the item is swiped away.
                background: Container(color: Colors.red),
                child: Container(
                  height: (height / 5.0),
                  child: Card(
                      elevation: 3.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 120.0,
                                height: ((height - 200.0) / 4.0),
                                  child:Image.network(PRODUCT_IMAGE_URL+item.photo)
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            width: (width - 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '${item.name}',
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Price:',
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text(
                                      'R${item.price}',
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    RichText(
                                      text: TextSpan(
                                        text: 'Size:  ',
                                        style: TextStyle(
                                            fontSize: 15.0, color: Colors.grey),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: '  ${item.size}',
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: Colors.blue)),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    InkWell(
                                      child: Container(
                                        color: Colors.grey,
                                        padding: EdgeInsets.all(3.0),
                                        child: Text(
                                          'Remove',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          wishlistItem--;
                                        });

                                        // Then show a snackbar.
                                        Scaffold.of(context).showSnackBar(
                                            SnackBar(
                                                content: Text("Item Removed")));
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ),
              );
            },
          );
        }),
      );
    }
  }
}
