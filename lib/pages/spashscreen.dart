import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/pages/home.dart';
import 'package:go_kart/pages/login.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferencesHelper sharedPreferencesHelper;
  User user;
  String x;
  String emailx;

  @override
  void initState() {
    super.initState();
    sharedPreferencesHelper = SharedPreferencesHelper();
    user = User();
    getUserId();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    if (x != null) {
      print('CHIKORO CHIKURU CHIKURU' + emailx);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home()),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }

  getUserId() async {
    x = await sharedPreferencesHelper.getUser("USER");
    user = User().fromMap(jsonDecode(x));
    emailx = user.email.toString();
    return user;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 50.0,
                        child: Image(
                          image: AssetImage(
                            'assets/mtc.png',
                          ),
                          width: 65.0,
                          height: 65.0,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 10.0),
                      ),
                      Text(
                        "Mtc Mall",
                        style: TextStyle(
                            fontFamily: 'Pacifico',
                            color: Colors.white,
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(
                      // backgroundColor: Colors.yellow,
                      valueColor: new AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColorLight),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20.0),
                    ),
                    Text(
                      'No 1 Online store \nIn Africa',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
