import 'package:flutter/material.dart';
import 'package:go_kart/Animation/slide_left_rout.dart';
import 'package:go_kart/constants/Constant.dart';
import 'package:go_kart/pages/order_payment/delivery_address.dart';
import 'package:go_kart/viewModel/cart_view_model.dart';
import 'package:provider/provider.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  int cartItem = 3;
  int cartTotal = 1797;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width * 0.7;
    double widthFull = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    Provider.of<CartViewModel>(context, listen: false).getCartByUserId();
    {
      // No Item in Cart AlertDialog Start Here
      void _showDialog() {
        // flutter defined function
        showDialog(
          context: context,
          builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
              title: Text(
                "Alert",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              content: Text("No Item in Cart"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: Text(
                    "Close",
                    style: TextStyle(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          },
        );
      }
      // No Item in Cart AlertDialog Ends Here

      return Scaffold(
        appBar: AppBar(
          title: Text('My Cart'),
          titleSpacing: 0.0,
          backgroundColor: Theme.of(context).primaryColor,
        ),
        bottomNavigationBar: Material(
          elevation: 5.0,
          child: Container(
            color: Colors.white,
            width: widthFull,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  width: ((widthFull) / 2),
                  height: 50.0,
                  alignment: Alignment.center,
                  child: RichText(
                    text: TextSpan(
                      text: 'Total: ',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 15.0,
                          color: Colors.black),
                      children: <TextSpan>[
                        TextSpan(
                            text: ' R$cartTotal',
                            style: TextStyle(
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue)),
                      ],
                    ),
                  ),
                ),
                ButtonTheme(
                  minWidth: ((widthFull) / 2),
                  height: 50.0,
                  child: RaisedButton(
                    child: Text(
                      'Pay Now',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                    onPressed: () {
                      (cartTotal == 0)
                          ? _showDialog()
                          : Navigator.push(
                              context, SlideLeftRoute(page: Delivery()));
                    },
                    color: (cartTotal == 0)
                        ? Colors.grey
                        : Theme.of(context).primaryColor,
                  ),
                ),
              ],
            ),
          ),
        ),
        body: Consumer<CartViewModel>(builder: (context, cartViewModel, child) {
          return ListView.builder(
            itemCount: cartViewModel.cartList.length,
            itemBuilder: (context, index) {
              final item = cartViewModel.cartList[index];
              return Dismissible(
                key: Key('$item'),
                onDismissed: (direction) {
                  setState(() {
                    cartViewModel.cartList.removeAt(index);
                    cartItem--;
                    cartTotal = cartTotal - 34;
                  });

                  // Then show a snackbar.
                  Scaffold.of(context)
                      .showSnackBar(SnackBar(content: Text("Item Removed")));
                },
                // Show a red background as the item is swiped away.
                background: Container(color: Colors.red),
                child: Container(
                  height: (height / 5.0),
                  child: Card(
                      elevation: 3.0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 120.0,
                                height: ((height - 200.0) / 4.0),
                                child:Image.network(PRODUCT_IMAGE_URL+item.photo)
                              ),
                            ],
                          ),
                          Container(
                            padding: EdgeInsets.all(10.0),
                            width: (width - 20.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  '${item.name}',
                                  style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      'Price:',
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text(
                                      'R${item.price}',
                                      style: TextStyle(
                                        color: Colors.blue,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 7.0,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    RichText(
                                      text: TextSpan(
                                        text: 'Size:  ',
                                        style: TextStyle(
                                            fontSize: 15.0, color: Colors.grey),
                                        children: <TextSpan>[
                                          TextSpan(
                                              text: '  ${item.size}',
                                              style: TextStyle(
                                                  fontSize: 15.0,
                                                  color: Colors.blue)),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    InkWell(
                                      child: Container(
                                        color: Colors.grey,
                                        padding: EdgeInsets.all(3.0),
                                        child: Text(
                                          'Remove',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          cartViewModel.cartList
                                              .removeAt(index);
                                          cartItem--;
                                          cartTotal = cartTotal - 23;
                                        });

                                        // Then show a snackbar.
                                        Scaffold.of(context).showSnackBar(
                                            SnackBar(
                                                content: Text("Item Removed")));
                                      },
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ),
              );
            },
          );
        }),
      );
    }
  }
//
//  Center(
//  child: Column(
//  mainAxisAlignment: MainAxisAlignment.center,
//  crossAxisAlignment: CrossAxisAlignment.center,
//  children: <Widget>[
//  Icon(
//  FontAwesomeIcons.shoppingBasket,
//  color: Colors.grey,
//  size: 60.0,
//  ),
//  SizedBox(
//  height: 10.0,
//  ),
//  Text(
//  'No Item in Cart',
//  style: TextStyle(color: Colors.grey),
//  ),
//  SizedBox(
//  height: 10.0,
//  ),
//  FlatButton(
//  child: Text(
//  'Go To Home',
//  style: TextStyle(color: Colors.white),
//  ),
//  color: Theme.of(context).primaryColor,
//  onPressed: () {
//  Navigator.push(
//  context,
//  MaterialPageRoute(builder: (context) => Home()),
//  );
//  },
//  )
//  ],
//  ),
//  )
}
