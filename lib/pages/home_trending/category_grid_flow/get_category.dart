import 'package:flutter/material.dart';
import 'package:go_kart/pages/category/top_offers_pages/filter_row.dart';

import 'category_grid_view.dart';

class GetCategory extends StatefulWidget {
  @override
  _GetCategoryState createState() => _GetCategoryState();
}

class _GetCategoryState extends State<GetCategory> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        FilterRow(),
        Divider(
          height: 1.0,
        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: CategoryGridView()),
      ],
    );
  }
}
