import 'package:flutter/material.dart';
import 'package:go_kart/pages/category/top_offers_pages/filter_row.dart';

import 'best_seller_grid_view.dart';

class GetBestSeller extends StatefulWidget {
  @override
  _BestSellerState createState() => _BestSellerState();
}

class _BestSellerState extends State<GetBestSeller> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        FilterRow(),
        Divider(
          height: 1.0,
        ),
        Container(
            margin: EdgeInsets.only(top: 10.0), child: BestSellerGridView()),
      ],
    );
  }
}
