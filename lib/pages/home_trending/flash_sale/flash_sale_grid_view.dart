import 'package:flutter/material.dart';
import 'package:go_kart/constants/Constant.dart';
import 'package:go_kart/viewModel/product_view_model.dart';
import 'package:provider/provider.dart';

import 'flash_product_detail.dart';

class FlashSaleGridView extends StatefulWidget {
  @override
  _FlashSaleGridViewState createState() => _FlashSaleGridViewState();
}

class _FlashSaleGridViewState extends State<FlashSaleGridView> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    Provider.of<ProductViewModel>(context, listen: false).getFlashSale();
    {
      return Container(
        margin: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10.0),
          boxShadow: [
            BoxShadow(
              blurRadius: 5.0,
              color: Colors.grey,
            ),
          ],
        ),
        child: Consumer<ProductViewModel>(
            builder: (context, productViewModel, child) {
          return GridView.count(
            shrinkWrap: true,
            primary: false,
            crossAxisSpacing: 0,
            mainAxisSpacing: 0,
            crossAxisCount: 2,
            childAspectRatio: ((width) / (height - 150.0)),
            children:
                List.generate(productViewModel.productList.length, (index) {
              final item = productViewModel.productList[index];

              return InkWell(
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => FlashSaleProductDetail(item: item),
                    ));
                  },
                  child: Container(
                    margin: EdgeInsets.all(5.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 5.0,
                          color: Colors.grey,
                        ),
                      ],
                    ),
                    child: Column(
                      children: <Widget>[
                        Hero(
                          tag: '${item.name}',
                          child: Container(
                            // height: 185.0,
                            height: ((height - 150.0) / 2.95),
                            margin: EdgeInsets.all(6.0),
                              child: Image.network(PRODUCT_IMAGE_URL+item.photo)
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(right: 6.0, left: 6.0),
                          child: Column(
                            children: <Widget>[
                              Text(
                                item.name,
                                style: TextStyle(fontSize: 12.0),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                                textAlign: TextAlign.center,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    "R${item.price}",
                                    style: TextStyle(fontSize: 16.0),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                  SizedBox(
                                    width: 7.0,
                                  ),
                                  Text(
                                    "R${item.previous_price}",
                                    style: TextStyle(
                                        fontSize: 13.0,
                                        decoration: TextDecoration.lineThrough,
                                        color: Colors.grey),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              Text(
                                "70% off",
                                style: TextStyle(
                                    color: const Color(0xFF67A86B),
                                    fontSize: 14.0),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ));
            }),
          );
        }),
      );
    }
  }
}
