import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/pages/home_trending/category_grid_flow/category_container.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';
import 'package:go_kart/viewModel/category_view_model.dart';
import 'package:provider/provider.dart';

class CategoryGrid extends StatelessWidget {
  final SharedPreferencesHelper sharedPreferencesHelper =
      SharedPreferencesHelper();

  @override
  Widget build(BuildContext context) {
    Provider.of<CategoryViewModel>(context, listen: false).getAllCategories();
    {
      return Container(
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        padding: EdgeInsets.all(5.0),
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width - 20.0,
        height: 200.0,
        child: Consumer<CategoryViewModel>(
            builder: (context, categoryViewModel, child) {
          return GridView.count(
            primary: false,
            padding: const EdgeInsets.all(0),
            crossAxisSpacing: 0,
            mainAxisSpacing: 15,
            crossAxisCount: 4,
            children:
                List.generate(categoryViewModel.categoryList.length, (index) {
              final item = categoryViewModel.categoryList[index];

              return InkWell(
                onTap: () {
                  sharedPreferencesHelper
                      .saveCategory(
                          "CATEGORYID", jsonEncode(item.toJson()).toString())
                      .then((isSaved) {
                    if (isSaved) {
                      print("succefully saved" +
                          jsonEncode(item.toJson()).toString());
                    } else {
                      print("not saved" + jsonEncode(item.toJson()).toString());
                    }
                  }).catchError((onError) {
                    print("oops something went wrong" + onError);
                  });
                  Navigator.of(context).push(new MaterialPageRoute(
                    builder: (c) => CategoryContainer(item: item),
                  ));
                },
                child: Image(
                  image: AssetImage(item.image),
                  fit: BoxFit.fitHeight,
                ),
              );
            }),
          );
        }),
      );
    }
  }
}
