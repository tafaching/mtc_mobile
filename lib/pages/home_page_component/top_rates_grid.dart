import 'package:flutter/material.dart';
import 'package:go_kart/constants/Constant.dart';
import 'package:go_kart/pages/home_trending/top_rates/top_rates.dart';
import 'package:go_kart/pages/home_trending/top_rates/top_rates_product_detail.dart';
import 'package:go_kart/viewModel/product_view_model.dart';
import 'package:provider/provider.dart';

class TopRatesGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      width: double.infinity,
      // height: 500.0,
      child: Column(
        children: <Widget>[
          TopTitle(),
          SizedBox(
            height: 5.0,
          ),
          BestOfferItems(),
        ],
      ),
    );
  }
}

class BestOfferItems extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    Provider.of<ProductViewModel>(context, listen: false).getTopRates();
    {
      return Column(
        children: <Widget>[
          SizedBox(
            width: width,
            height: 180.0,
            child: Stack(
              children: <Widget>[
                Container(
                  height: 100.0,
                  decoration: BoxDecoration(color: Colors.white),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                  ),

                  alignment: Alignment.center,
                  // Todo  this is card height
                  height: 180.0,
                  child: Consumer<ProductViewModel>(
                      builder: (context, productViewModel, child) {
                    return ListView(
                      scrollDirection: Axis.horizontal,
                      primary: false,
                      padding: const EdgeInsets.all(0),
                      children: List.generate(
                          productViewModel.productList.length, (index) {
                        final item = productViewModel.productList[index];

                        return InkWell(
                          onTap: () {
                            Navigator.of(context).push(new MaterialPageRoute(
                              builder: (c) => TopRatesProductDetail(
                                item: item,
                              ),
                            ));
                          },
                          child: Container(
                            width: 160,
                            height: 100,
                            margin: EdgeInsets.all(5.0),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10.0),
                              boxShadow: [
                                BoxShadow(
                                  blurRadius: 5.0,
                                  color: Colors.grey,
                                ),
                              ],
                            ),
                            child: Column(
                              children: <Widget>[
                                Container(
                                    margin: EdgeInsets.all(6.0),
                                    height: 100.0,
                                    child: Image.network(
                                        PRODUCT_IMAGE_URL + item.photo)),
                                Container(
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        '${item.name}',
                                        style: TextStyle(fontSize: 12.0),
                                        textAlign: TextAlign.center,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Text(
                                            "R${item.price}",
                                            style: TextStyle(
                                                fontSize: 14.0,
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(
                                            width: 7.0,
                                          ),
                                          Text(
                                            "R${item.previous_price}",
                                            style: TextStyle(
                                                fontSize: 13.0,
                                                decoration:
                                                    TextDecoration.lineThrough,
                                                color: Colors.grey),
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            textAlign: TextAlign.center,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                    );
                  }),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }
}

class TopTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Image(
          image: AssetImage('assets/top_design/white.png'),
        ),
        Positioned(
          top: 10.0,
          left: 20.0,
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(0.0),
            width: 320.0,
            height: 20.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  'Top Ratings',
                  style: TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                InkWell(
                  child: Text(
                    'View All',
                    style: TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                  onTap: () {
                    Navigator.of(context).push(new MaterialPageRoute(
                      builder: (c) => TopRates(),
                    ));
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
