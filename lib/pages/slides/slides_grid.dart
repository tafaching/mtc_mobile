import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:go_kart/constants/Constant.dart';
import 'package:go_kart/viewModel/slides_view_model.dart';
import 'package:provider/provider.dart';

class SlidesGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    Provider.of<SlidesViewModel>(context, listen: false).getAllSlides();
    {
      return Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
            ),

            // Todo  this is card height
            height: 180.0,
            width: width,
            child: Consumer<SlidesViewModel>(
                builder: (context, productViewModel, child) {
              return InkWell(
                onTap: () {},
                child: CarouselSlider(
                  height: 180,
                  viewportFraction: 1.0,
                  items:
                      List.generate(productViewModel.slideList.length, (index) {
                    final item = productViewModel.slideList[index];
                    return Image.network(
                      SLIDER_URL + item.photo,
                      width: width,
                      fit: BoxFit.cover,
                    );
                  }),
                  autoPlay: true,
                  autoPlayInterval: const Duration(seconds: 4),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  autoPlayAnimationDuration: const Duration(milliseconds: 800),
                ),
              );
            }),
          ),
        ],
      );
    }
  }
}
