import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:go_kart/Animation/slide_left_rout.dart';
import 'package:go_kart/pages/cart/cartPage.dart';
import 'package:go_kart/pages/home_page_component/best_of_fashion.dart';
import 'package:go_kart/pages/home_page_component/best_seller_grid.dart';
import 'package:go_kart/pages/home_page_component/category_grid.dart';
import 'package:go_kart/pages/home_page_component/drawer.dart';
import 'package:go_kart/pages/home_page_component/featured_brands.dart';
import 'package:go_kart/pages/home_page_component/featured_grid.dart';
import 'package:go_kart/pages/home_page_component/flash_sale_grid.dart';
import 'package:go_kart/pages/home_page_component/top_rates_grid.dart';
import 'package:go_kart/pages/home_page_component/electronics_grid.dart';
import 'package:go_kart/pages/notifications.dart';
import 'package:go_kart/pages/slides/slides_grid.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Icon searchIcon = Icon(Icons.search);

  bool search = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: const Color(0xFFF1F3F6),
      appBar: AppBar(
        title: Text(
          'Mtc Mall',
          style: TextStyle(
            fontFamily: 'Pacifico',
          ),
        ),
        titleSpacing: 0.0,
        backgroundColor: Theme.of(context).primaryColor,
        actions: <Widget>[
          IconButton(
            icon: (!search) ? Icon(Icons.search) : Icon(Icons.close),
            color: Colors.white,
            onPressed: () {
              setState(() {
                if (!search) {
                  search = true;
                } else {
                  search = false;
                }
              });
            },
          ),
          IconButton(
            icon: Badge(
              badgeContent: Text('2'),
              badgeColor: Theme.of(context).primaryColorLight,
              child: Icon(
                Icons.notifications,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Notifications()),
              );
            },
          ),
          IconButton(
            icon: Badge(
              badgeContent: Text('3'),
              badgeColor: Theme.of(context).primaryColorLight,
              child: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              Navigator.push(context, SlideLeftRoute(page: CartPage()));
            },
          ),
        ],
        bottom: PreferredSize(
          preferredSize: (!search) ? Size(0.0, 0.0) : Size(width, 50.0),
          child: (!search)
              ? Container()
              : Container(
                  alignment: Alignment.center,
                  child: Container(
                    width: width - 20.0,
                    color: Colors.white,
                    margin: EdgeInsets.only(bottom: 5.0),
                    alignment: Alignment.center,
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: 'Search Here',
                          prefixIcon: Icon(Icons.search),
                          suffixIcon: IconButton(
                            icon: Icon(Icons.close),
                            onPressed: () {
                              setState(() {
                                if (!search) {
                                  search = true;
                                } else {
                                  search = false;
                                }
                              });
                            },
                          )),
                      textAlignVertical: TextAlignVertical.center,
                    ),
                  ),
                ),
        ),
      ),

      // Drawer Code Start Here

      drawer: MainDrawer(),

      // Drawer Code End Here
      body: ListView(
        children: <Widget>[
          // Slider Code Start Here
          SlidesGrid(),
//          Test(),
          // Slider Code End Here

          SizedBox(
            height: 5.0,
          ),

          // Category Grid Start Here
          //TODO----Remove category for now
//          CategoryGrid(),

          // Category Grid End Here

//          SizedBox(
//            height: 5.0,
//          ),

          Divider(
            height: 1.0,
          ),

          SizedBox(
            height: 4.0,
          ),

          // Promotion 1 Start Here
          InkWell(
            onTap: () {
//              Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (context) =>
//                        TopOffers(ite,: 'Top Selling Android Mobile')),
//              );
            },
            child: Image(
              image: AssetImage('assets/promotion/promotion1.jpg'),
              fit: BoxFit.cover,
            ),
          ),
          // Promotion 1 End Here

          SizedBox(
            height: 5.0,
          ),

          Divider(
            height: 1.0,
          ),

          SizedBox(
            height: 2.0,
          ),

          // Best Offer Grid Start Here

          FlashSaleGrid(),

          // Best Offer Grid End Here
//          SizedBox(
//            height: 4.0,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 4.2,
//          ),
//
//          // Top Seller Grid Start Here
//          BestSellerGrid(),
//          // Top Seller Grid End Here

//          SizedBox(
//            height: 3.8,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 4.0,
//          ),
//
//          // Best Deal Grid Start Here
//          TopRatesGrid(),
//          // Best Deal Grid End Here
//
//          SizedBox(
//            height: 3.8,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 8.0,
//          ),
//
//          // Featured Brand Slider Start Here
//          FeaturedBrandSlider(),
//          // Featured Brand Slider End Here
//
//          SizedBox(
//            height: 6.0,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 6.0,
//          ),
//
//          // Block Buster Deals Start Here
//          FlashSaleGrid(),
//          // Block Buster Deals End Here
//
//          SizedBox(
//            height: 6.0,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 0.0,
//          ),
//
//          //Best of Fashion Start Here
//
//
//          (),
//          //Best of Fashion End Here
//
//          SizedBox(
//            height: 6.0,
//          ),
//
//          Divider(
//            height: 1.0,
//          ),
//
//          SizedBox(
//            height: 0.0,
//          ),
//
//          // Womens Collection Start Here
//          BestOfElectronics(),
          // Womens Collection End Here
        ],
      ),
    );
  }
}
