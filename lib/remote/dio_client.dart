import 'package:dio/dio.dart';

import 'headers_interceptor.dart';

class DioClient {
  static Dio getDioClient() {
    return Dio()
      //..options = BaseOptions(baseUrl: 'http://10.0.2.2:8888/mtc/')
      // ..options = BaseOptions(baseUrl: 'http://127.0.0.1:8888/mtc/')
      ..options = BaseOptions(baseUrl: 'http://g40property.co.za/mall/mtc/')
      ..interceptors.add(HeadersInterceptor())
      ..interceptors.add(LogInterceptor(responseBody: true, requestBody: true));
  }
}
