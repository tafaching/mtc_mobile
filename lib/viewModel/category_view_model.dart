import 'package:flutter/material.dart';
import 'package:go_kart/model/category.dart';
import 'package:go_kart/repository/generic_repository.dart';

class CategoryViewModel extends ChangeNotifier with ACategoryViewModel {
  String categoryId = 'categoryId/{categoryId}';
  String allCategories = 'assets/category/category.json';

  List<Category> categoryList = [];

  @override
  Future getAllCategories() async {
    //get Group
    await GenericRepository<Category>()
        .getAll(allCategories, Category(), null, null)
        .then((response) {
      categoryList = response;
    });
    notifyListeners();
  }

  @override
  void getCategoryById(int id) async {
    //get properties
    await GenericRepository<Category>().getAll(
        categoryId, Category(), null, {'categoryId': id}).then((response) {
      categoryList = response;
    });
    notifyListeners();
  }
}

abstract class ACategoryViewModel {
  void getAllCategories();

  void getCategoryById(int id);
}
