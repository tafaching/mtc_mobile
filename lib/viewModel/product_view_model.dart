import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/category.dart';
import 'package:go_kart/model/productModel.dart';
import 'package:go_kart/repository/generic_repository.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class ProductViewModel extends ChangeNotifier with AProductViewModel {
  String categoryId = 'api/productByCategoryId/{id}';
  String featured = 'api/featuredProduct/{id}';
  String bestSeller = 'api/bestSellerProduct/{id}';
  String topSeller = 'api/topSellerProduct/{id}';
  String flashSale = 'api/flashSaleProduct/{id}';
  String allProducts = 'all';

  List<ProductModel> productList = [];

  @override
  Future getAllProducts() async {
    await GenericRepository<ProductModel>()
        .getAll(allProducts, ProductModel(), null, null)
        .then((response) {
      productList = response;
    });
    notifyListeners();
  }

  @override
  void getProductByCategoryId(int id) async {
    await GenericRepository<ProductModel>()
        .getAll(categoryId, ProductModel(), null, {'id': id}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  // TODO -----ONCLICK CATEGORY LIST ON HOME PAGE
  @override
  void getProductByCaterId() async {
    final String x = await SharedPreferencesHelper().getCategory("CATEGORYID");
    print("CATEGORY  IYO" + x);
    Category category = Category().fromMap(jsonDecode(x));
    print("CATEGORY  ID" + category.id.toString());
    print(category);

    //get properties
    await GenericRepository<ProductModel>().getAll(
        categoryId, ProductModel(), null, {'id': category.id}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  void getFeatured() async {
    await GenericRepository<ProductModel>()
        .getAll(featured, ProductModel(), null, {'id': 1}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  void getBestSeller() async {
    await GenericRepository<ProductModel>()
        .getAll(bestSeller, ProductModel(), null, {'id': 0}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  void getTopRates() async {
    await GenericRepository<ProductModel>()
        .getAll(topSeller, ProductModel(), null, {'id': 0}).then((response) {
      productList = response;
    });
    notifyListeners();
  }

  void getFlashSale() async {
    await GenericRepository<ProductModel>()
        .getAll(flashSale, ProductModel(), null, {'id': 1}).then((response) {
      productList = response;
    });
    notifyListeners();
  }
}

abstract class AProductViewModel {
  void getAllProducts();

  void getProductByCaterId();

  void getBestSeller();

  void getFeatured();

  void getFlashSale();

  void getTopRates();

  void getProductByCategoryId(int id);
}
