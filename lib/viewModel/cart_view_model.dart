import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/cart.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/repository/generic_repository.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class CartViewModel extends ChangeNotifier with ACategoryViewModel {
  String getCart = 'api/getCart/{user_id}';
  String deleteCart = 'api/cart/{user_id}';
  String deleteItem = 'api/item/{id}';
  Cart cart;
  List<Cart> cartList = [];
  SharedPreferencesHelper sharedPreferencesHelper;

  @override
  void getCartByUserId() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("Cart  IYO" + x);
    User user = User().fromMap(jsonDecode(x));
    print("Cart  ID" + user.id.toString());
    print(user);

    //TODO REMOVE MAGIC STRING USER ID

    await GenericRepository<Cart>()
        .getAll(getCart, Cart(), null, {'user_id': "31"}).then((response) {
      cartList = response;
    });
    notifyListeners();
  }

  @override
  void deleteItemById() async {
    //get properties
    await GenericRepository<Cart>().deleteAll(
        deleteItem, Cart(), null, {'id': cart.userId}).then((response) {});
    notifyListeners();
  }

  @override
  void deleteCartByUserId() async {
    //get properties
    await GenericRepository<Cart>().deleteAll(
        deleteCart, Cart(), null, {'user_id': cart.userId}).then((response) {});
    notifyListeners();
  }
}

abstract class ACategoryViewModel {
  void getCartByUserId();

  void deleteCartByUserId();

  void deleteItemById();
}
