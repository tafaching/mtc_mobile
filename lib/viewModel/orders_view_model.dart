import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/order.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/repository/generic_repository.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class OrdersViewModel extends ChangeNotifier with AOrdersViewModel {
  String userId = 'api/orderByUserId/{id}';
  String allProducts = 'all';
  SharedPreferencesHelper sharedPreferencesHelper;

  List<Order> orderList = [];

  @override
  Future getAllOrders() async {
    await GenericRepository<Order>()
        .getAll(allProducts, Order(), null, null)
        .then((response) {
      orderList = response;
    });
    notifyListeners();
  }

  @override
  void getOrderByUserId() async {
    final String x = await SharedPreferencesHelper().getCategory("USER");
    print("USER  IYO" + x);
    User user = User().fromMap(jsonDecode(x));
    print("USER  ID" + user.id.toString());
    print(user);

    //todo remove id magic string
    //get properties
    await GenericRepository<Order>()
        .getAll(userId, Order(), null, {'id': "31"}).then((response) {
      orderList = response;
    });
    notifyListeners();
  }
}

abstract class AOrdersViewModel {
  void getAllOrders();

  void getOrderByUserId();
}
