import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/model/wish.dart';
import 'package:go_kart/repository/generic_repository.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class WishViewModel extends ChangeNotifier with ACategoryViewModel {
  String getCart = 'api/getWish/{user_id}';
  String deleteCart = 'api/wish/{user_id}';
  String deleteItem = 'api/item/{id}';
  Wish wish;
  List<Wish> wishList = [];
  SharedPreferencesHelper sharedPreferencesHelper;

  @override
  void getWishByUserId() async {
    final String x = await SharedPreferencesHelper().getUser("USER");
    print("wish  IYO" + x);
    User user = User().fromMap(jsonDecode(x));
    print("wish  ID" + user.id.toString());
    print(user);

    //TODO REMOVE MAGIC STRING USER ID

    await GenericRepository<Wish>()
        .getAll(getCart, Wish(), null, {'user_id': "31"}).then((response) {
      wishList = response;
    });
    notifyListeners();
  }

  @override
  void deleteItemById() async {
    //get properties
    await GenericRepository<Wish>().deleteAll(
        deleteItem, Wish(), null, {'id': wish.user_id}).then((response) {});
    notifyListeners();
  }

  @override
  void deleteWishByUserId() async {
    //get properties
    await GenericRepository<Wish>().deleteAll(deleteCart, Wish(), null,
        {'user_id': wish.user_id}).then((response) {});
    notifyListeners();
  }
}

abstract class ACategoryViewModel {
  void getWishByUserId();

  void deleteWishByUserId();

  void deleteItemById();
}
