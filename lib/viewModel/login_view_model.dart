import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/repository/generic_repository.dart';
import 'package:go_kart/sharedPref/shared_preferences_helper.dart';

class LoginViewModel extends ChangeNotifier with ALognViewModel {
  String loginPoint = 'api/login';

  SharedPreferencesHelper sharedPreferencesHelper=SharedPreferencesHelper();

  @override
  void logOut() {
    // TODO: implement logOut
  }

  @override
  void login(User user, LoginListener loginListener) async {
    String userJson;
    await GenericRepository<User>()
        .postResponseList(loginPoint, user, null, null)
        .then((response) async {
      userJson = jsonEncode(response[0].toJson());
      print(userJson);
      print("TAFADZWA" + userJson);
    }).catchError((onError) {
      loginListener.onFail();
    });

    await sharedPreferencesHelper.saveUser("USER", userJson).then((isSaved) {
      if (isSaved) {

        print("succefully saved" + userJson);
        loginListener.onSuccess();
      } else {
        print("not saved" + userJson);
      }
    }).catchError((onError) {
      print("oops something went wrong" + onError);
    });
  }
}

abstract class LoginListener {
  void onSuccess();

  void onFail();
}

abstract class ALognViewModel {
  void login(User user, LoginListener loginListener);

  void logOut();
}
