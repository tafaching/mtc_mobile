import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:go_kart/model/user.dart';
import 'package:go_kart/repository/generic_repository.dart';

class RegistrationViewModel extends ChangeNotifier with ARegisterViewModel {
  String loginPoint = 'api/register';
  User x = User();

  @override
  void register(User user, RegistrationListener registrationListener) async {
    await GenericRepository<User>()
        .post(loginPoint, user, null, null)
        .then((response) {
      String userJson = jsonEncode(user.toJson()).toString();
      print('Status Code: ' +
          response.statusCode.toString() +
          " UserJson" +
          userJson);
      registrationListener.onSuccess();
    }).catchError((onError) {
      registrationListener.onFail();
    });
  }
}

abstract class RegistrationListener {
  void onSuccess();

  void onFail();
}

abstract class ARegisterViewModel {
  void register(User user, RegistrationListener registrationListener);
}
