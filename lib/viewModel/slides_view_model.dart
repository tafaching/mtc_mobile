import 'package:flutter/material.dart';
import 'package:go_kart/model/Slides.dart';
import 'package:go_kart/model/category.dart';
import 'package:go_kart/repository/generic_repository.dart';

class SlidesViewModel extends ChangeNotifier with ASlidesViewModel {
  String allSlides = 'api/allSlides';

  List<Slides> slideList = [];

  @override
  Future getAllSlides() async {

    await GenericRepository<Slides>()
        .getAll(allSlides, Slides(), null, null)
        .then((response) {
      slideList = response;
    });
    notifyListeners();
  }
}

abstract class ASlidesViewModel {
  void getAllSlides();
}
